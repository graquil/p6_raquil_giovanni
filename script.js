class Carousel{

    /**
     * This callback type is called moveCallback and is display as a global symbol.
     *  
     * @callback moveCallback
     * @param {number} index 
     */

    /**
     * class pour la creation des carousels
     * @param {HTMLElement} element 
     * @param {Object} option 
     * @param {Object} option.slidesToScroll Nbr d'élément a faire défiler
     * @param {Object} option.slidesVisibles Nbr d'élément visible dans un slide
     * @param {boolean} option.loop Doit-on boucler en fin de carrousel
     */

constructor(element, option = {}) {
    this.element = element
    this.option = Object.assign({}, {
        slidesToScroll: 1,
        slidesVisibles:1,
        loop: false
    }, option)

    let children = [].slice.call(element.children)
    this.currentItem = 0
    this.root = this.createDivWithClass("carrousel")
    this.container = this.createDivWithClass("carrousel__container")
    this.root.appendChild(this.container)
    this.element.appendChild(this.root)
    this.moveCallbacks = []
    this.items = children.map((child) => {
        let item = this.createDivWithClass('carrousel__item')
        item.appendChild(child)
        this.container.appendChild(item)
        return item
    })
    this.setStyle()
    this.createNavigation()
    this.moveCallbacks.forEach(cb => cb(0))
}

/**
 * définie les dimensions des éléments du carrousel
 */

setStyle() {
    let ratio = this.items.length / this.option.slidesVisibles;
    this.container.style.width = (ratio * 100) + "%";
    this.items.forEach(item => item.style.width = ((100 / this.option.slidesVisibles) / ratio) + "%");

}
/**
 * creation et fonctionnement des boutons de déplacement dans le carrousel
 * @returns 
 */
createNavigation() {
    let nextButton = this.createDivWithClass("carrousel__next")
    let prevButton = this.createDivWithClass("carrousel__prev")
    this.root.appendChild(nextButton)
    this.root.appendChild(prevButton)
    nextButton.addEventListener("click", this.next.bind(this))
    prevButton.addEventListener("click", this.prev.bind(this))
    if (this.option.loop === true){
        return
    }
    this.onMove(index => {
        if (index === 0) {
            prevButton.classList.replace("carrousel__prev", "carrousel__prev--hidden")
        }else {
            prevButton.classList.replace("carrousel__prev--hidden","carrousel__prev")
        }
        if (this.items[this.currentItem + this.option.slidesVisibles] === undefined){
            nextButton.classList.replace("carrousel__next", "carrousel__next--hidden")
        }else {
            nextButton.classList.replace("carrousel__next--hidden","carrousel__next")
        }
    })
}

next() {
    this.goToItem(this.currentItem + this.option.slidesToScroll)
    
}

prev() {
    this.goToItem(this.currentItem - this.option.slidesToScroll)
}

/**
 * deplace le carrousel vers l'élément ciblé
 * @param {number} index 
 */
goToItem(index) {
    if (index< 0) {
        index = this.items.length - this.option.slidesVisibles
    }else if (index >= this.items.length || (this.items[this.currentItem + this.option.slidesVisibles] === undefined && index > 
        this.currentItem)){
        index = 0
    }
    let translateX = index * -100 / this.items.length
    this.container.style.transform = 'translate3d(' + translateX + '% , 0, 0)'
    this.currentItem = index
    this.moveCallbacks.forEach(cb => cb(index))
}
/**
 * 
 * @param {moveCallback} cb 
 */
onMove(cb) {
    this.moveCallbacks.push(cb)

}

/**
 * creation d'élément html div
 * @param {string} className 
 * @return (HTMLElement)
 */
createDivWithClass(className) {
    let div = document.createElement("div")
    div.setAttribute("class", className)
    return div
}
}

const ajax = async (url)=> {
    const res = await fetch(url)
    if (res.ok){
        const data = await res.json()
        console.log(data)
        return data
    }
}

/**
 * permet d'avoir les informations du film passer en url 
 * @param {string} url du film 
 * @returns dictionnaire des informations du film
 */
const film_data = async (url)=> {
    const data_film = await ajax(url)
    return data_film
    }

/**
 * permet d'avoir la liste des 7 films d'une catégorie
 * @param {string} url de la page de la catégorie 
 * @returns liste de promise des 7 1er films
 */
const seven_films = async (url)=> {
    let url_films = await ajax(url)
    const seven_film = url_films.results.slice(3)
    return seven_film
}

/**
 * permet d'afficher les informations du meilleur film 
 * @param {string} url du meilleur film 
 */
const populate_best_film = async (url)=> {
    url_films = await ajax(url)
    url = url_films.results[0].url
    const data_film = await film_data(url)
    

    const img = document.getElementById("best_film_img")
    img.src = data_film.image_url

    const title = document.querySelector("#best_film .data_film .film_title")
    title.innerText = data_film.title

    const description = document.querySelector("#best_film .data_film .description")
    description.innerText = data_film.long_description

    const btn =  document.querySelector(".data_film .bouton")
    btn.addEventListener("click", () => {
        open_modal_windows(data_film.url)} )
}

/**
 * permet d'ajouter les images dans les carrousels
 * @param {string} url 
 * @param {HTMLElement} balise_img 
 */
const populate_img_film = async (url, balise_img)=>{
    const films = await seven_films(url)
    for (let step = 0; step < 7; step++){
        balise_img[step].src = films[step].image_url
        balise_img[step].value = films[step].url
        
    }
}

/**
 * permet d'ajouter les informations d'un film dans la fenetre modale
 * @param {string} url url du film 
 */
const populate_film = async (url)=>{
    const data_film = await film_data(url);

    const img = document.getElementById("img_film")
    const title = document.getElementById("titre");
    const description = document.getElementById("description");
    const genre = document.getElementById("genre");
    const date_de_sortie = document.getElementById("date_de_sortie");
    const score = document.getElementById("score");
    const score_imdb = document.getElementById("score_imdb");
    const realisateur = document.getElementById("realisateur");
    const liste_des_acteurs = document.getElementById("liste_des_acteurs");
    const duree = document.getElementById("duree");
    const pays = document.getElementById("pays");
    const score_box_office = document.getElementById("score_box_office");

    title.innerText = data_film.title;
    img.src = data_film.image_url;
    description.innerText = data_film.long_description;
    genre.innerText = data_film.genres;
    date_de_sortie.innerText = data_film.date_published;
    score.innerText = data_film.rated;
    score_imdb.innerText = data_film.imdb_score;
    realisateur.innerText = data_film.directors.join(", ");
    liste_des_acteurs.innerText = data_film.actors.join(", ");
    duree.innerText = data_film.duration;
    pays.innerText = data_film.countries.join(", ");
    score_box_office.innerText = data_film.worldwide_gross_income;

}

/**
 * permet d'ouvrir la fenetre modal
 * @param {string} url du film 
 */
const open_modal_windows = async (url)=> {
    const btn_close = document.querySelector(".info_film .bouton")
    let dialog = document.querySelector(".info_film")
    dialog.style.visibility = "visible"
    populate_film(url)
    if(typeof dialog.showModal === "function") {
        dialog.showModal();
    } else{
        console.error("L'API <dialog> n'est pas prise en charge par ce navigateur.");
    }
    btn_close.addEventListener("click", function onClose(){
        dialog.style.visibility = "hidden"
        dialog.close()
    })
}

const url_best_film = "http://localhost:8000/api/v1/titles/?sort_by=-imdb_score&page_size=10&page=1"
const url_catégorie1 = "http://localhost:8000/api/v1/titles/?sort_by=-imdb_score&page_size=10&page=1&genre=action&?"
const url3_catégorie2 = "http://localhost:8000/api/v1/titles/?sort_by=-imdb_score&page_size=10&page=1&genre=drama&?"
const url4_catégorie3 = "http://localhost:8000/api/v1/titles/?sort_by=-imdb_score&page_size=10&page=1&genre=thriller&?"

imgs = document.getElementsByClassName("img_carrousel1")
imgs2 = document.getElementsByClassName("img_carrousel2")
imgs3 = document.getElementsByClassName("img_carrousel3")
imgs4 = document.getElementsByClassName("img_carrousel4")

window.addEventListener("DOMContentLoaded",populate_best_film(url_best_film));
window.addEventListener("DOMContentLoaded",populate_img_film(url_best_film, imgs));
window.addEventListener("DOMContentLoaded",populate_img_film(url_catégorie1, imgs2));
window.addEventListener("DOMContentLoaded",populate_img_film(url3_catégorie2, imgs3));
window.addEventListener("DOMContentLoaded",populate_img_film(url4_catégorie3, imgs4));

document.addEventListener("DOMContentLoaded", function () {
    new Carousel(document.querySelector("#carrousel1"), {
    slidesToScroll: 4,
    slidesVisibles: 4,
    loop:false
    })
    new Carousel(document.querySelector("#carrousel2"), {
    slidesToScroll: 2,
    slidesVisibles: 4,
    loop:false
    })
    new Carousel(document.querySelector("#carrousel3"), {
    slidesToScroll: 2,
    slidesVisibles: 4,
    loop:false
    })
    new Carousel(document.querySelector("#carrousel4"), {
    slidesToScroll: 2,
    slidesVisibles: 4,
    loop:false
    })
    
    })

